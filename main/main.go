package main

import (
	"log"
	"net/http"
	"github.com/gorilla/mux"
)

func GetUsers(w http.ResponseWriter, r *http.Request)(//получаем юзера
	w.Write([]byte("Get users"))
)

func CreateUser(w http.ResponseWriter, r *http.Request)(//создаем юзера
	w.Write([]byte("Create user"))
)

func GetUser(w http.ResponseWriter, r *http.Request)(//получаем пользователя
	vars := mux.Vars(r)
	w.Write([]byte("Create user" + vars["id"]))
)

func DeleteUser(w http.ResponseWriter, r *http.Request)(//удаляем пользователя
	vars := mux.Vars(r)
	w.Write([]byte("Delete user" + vars["id"]))
)

func UpdateUser(w http.ResponseWriter, r *http.Request)(//изменяем пользователя
	vars := mux.Vars(r)
	w.Write([]byte("Update user" + vars["id"]))
)

//-===================================================

func GetDoctors(w http.ResponseWriter, r *http.Request)(//получаем врача
	w.Write([]byte("Get doctors"))
)

func CreateDoctor(w http.ResponseWriter, r *http.Request)(//создаем врача
	w.Write([]byte("Create doctor"))
)

func GetDoctor(w http.ResponseWriter, r *http.Request)(//получаем работника врача
	vars := mux.Vars(r)
	w.Write([]byte("Create doctor" + vars["id"]))
)

func DeleteDoctor(w http.ResponseWriter, r *http.Request)(//удаляем работника врача
	vars := mux.Vars(r)
	w.Write([]byte("Delete doctor" + vars["id"]))
)

func UpdateDoctors(w http.ResponseWriter, r *http.Request)(//изменяем работника врача
	vars := mux.Vars(r)
	w.Write([]byte("Update doctors" + vars["id"]))
)

//-===================================================

func GetAppointments(w http.ResponseWriter, r *http.Request)(//получаем запись
	w.Write([]byte("Get appointments"))
)

func CreateAppointment(w http.ResponseWriter, r *http.Request)(//создаем запись
	w.Write([]byte("Create appointment"))
)

func GetAppointment(w http.ResponseWriter, r *http.Request)(//получаем прием к врачу
	vars := mux.Vars(r)
	w.Write([]byte("Create appointment" + vars["id"]))
)

func DeleteAppointment(w http.ResponseWriter, r *http.Request)(//удаляем прием к врачу
	vars := mux.Vars(r)
	w.Write([]byte("Delete appointment" + vars["id"]))
)

func UpdateAppointments(w http.ResponseWriter, r *http.Request)(//изменяем прием к врачу
	vars := mux.Vars(r)
	w.Write([]byte("Update appointments" + vars["id"]))
)


func main(){
	r := mux.NewRouter()
	r.HandleFunc("/users", GetUsers).Methods(http.MethodGet)
	r.HandleFunc("/users", CreateUser).Methods(http.MethodGet)
	r.HandleFunc("/users/{id}", GetUser).Methods(http.MethodGet)
	r.HandleFunc("/users/{id}", DeleteUser).Methods(http.MethodGet)
	r.HandleFunc("/users/{id}", UpdateUsers).Methods(http.MethodGet)
	r.HandleFunc("/doctors", GetDoctors).Methods(http.MethodGet)
	r.HandleFunc("/doctors", CreateDoctor).Methods(http.MethodGet)
	r.HandleFunc("/doctors/{id}", GetDoctor).Methods(http.MethodGet)
	r.HandleFunc("/doctors/{id}", DeleteDoctor).Methods(http.MethodGet)
	r.HandleFunc("/doctors/{id}", UpdateDoctors).Methods(http.MethodGet)
	r.HandleFunc("/appointment", GetAppointments).Methods(http.MethodGet)
	r.HandleFunc("/appointment", GetAppointment).Methods(http.MethodGet)
	r.HandleFunc("/appointment/{id}", GetAppointment).Methods(http.MethodGet)
	r.HandleFunc("/appointment/{id}", DeleteAppointment).Methods(http.MethodGet)
	r.HandleFunc("/appointment/{id}", UpdateAppointments).Methods(http.MethodGet)

	log.Fatal.ListenAndServe(":8000", nil)
}